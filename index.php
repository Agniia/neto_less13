<?php
	header('Content-Type: text/html; charset=utf-8');
     function myAutoloader($className)
    {
       $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
       $fileName = __DIR__.'/classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
       if(file_exists($fileName))
        {
    		include $fileName;
        }
    }
    spl_autoload_register('myAutoloader');
           
    $host = 'localhost';
//    $user = 'adm';
   // $pass = '123';
 //   $db = 'netology';
//    $table = 'tasks';
      $user = 'anshukova';
      $pass = 'neto1747';
      $db = 'global';
      $table = 'tasks';
	
    $dbInstance = new \DBManager\DBManager($host, $user, $pass, $db);
    $dataManager = new \DataManager\DataManager();
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
         $description = (isset($_POST['taskDescription']) && !empty($_POST['taskDescription']))?strip_tags($_POST['taskDescription']):'Странная задача';
         $is_done = (isset($_POST['isDone']) && !empty($_POST['isDone']))?(int)$_POST['isDone']:0;
         $date_added = date('Y-m-d');
         $dbInstance->setNewDataToTasks($description,$is_done,$date_added);
         header('Location: index.php');          
    }
    if(isset($_GET['doneNewTask']) && !empty($_GET['doneNewTask']))
    {
        $idTask = (isset($_GET['doneNewTask']) && !empty($_GET['doneNewTask']))? (int)($_GET['doneNewTask']):null;
        $isDone = (isset($_GET['isDone']) && !empty($_GET['isDone']))?(int)$_GET['isDone']:0;
        $dbInstance->changeTaskStatus($idTask,$isDone); 
        header('Location: index.php');          
    }
    if(isset($_GET['deleteTask']) && !empty($_GET['deleteTask']))
    {
        $idTask = (isset($_GET['deleteTask']) && !empty($_GET['deleteTask']))? (int)($_GET['deleteTask']):null;
        $dbInstance->deleteTask($idTask); 
        header('Location: index.php');          
    }
 ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Tasks</title>
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>   
    <h1 style="margin: 80px auto 70px; text-align: center;">Менеджер задач</h1>
    <div class="container-fluid">  
        <div class="row justify-content-center">
            <div class="col-8">
                    <h2 style="margin: 0px auto 30px; text-align: center;">Список задач</h2>
                    <?php              
                        $html = '';
                        $tasks =  $dbInstance->getAllTasks(); 
                        $dataManager->showAllTasks($tasks,$html);     
                        echo $html;
                  ?>
            </div>
            <div class="col-4">
                  <h2 style="margin: 0px auto 30px; text-align: center;">Добавить задачу</h2>
                  <?php              
                        $html = '';
                        $dataManager->showFormToAddTask($html); 
                        echo $html;
                  ?>
            </div>
        </div><!-- row -->
  </div><!-- container --> 
 </body>   
 </html>