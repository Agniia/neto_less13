<?php
namespace DataManager; 

class DataManager
{	
	function __construct()
	{
	}

	public function showFormToAddTask(&$htmlCode) 
	{
		$htmlCode .= '<form action="" method="post">';
		$htmlCode .= '<div class="form-group">';
		$htmlCode .= '<label for="taskDescription">Новая задача</label>';
		$htmlCode .= '<input type="text" class="form-control" id="taskDescription" placeholder="Новая задача" name="taskDescription">';
		$htmlCode .= '</div>';
		$htmlCode .= '<div class="form-check d-flex justify-content-between align-items-center">';
		$htmlCode .= '<div class="col-4"><input class="form-check-input" type="radio" name="isDone" id="isDoneTrue" value="1">';
		$htmlCode .= '<label class="form-check-label" for="isDone">Сделано</label></div>';
		$htmlCode .= '<div class="col-4"><input class="form-check-input" type="radio" name="isDone" id="isDoneFalse" value="0" checked>';
		$htmlCode .= '<label class="form-check-label" for="isDoneFalse">Не сделано</label></div><div class="col-4"><button type="submit" name="addNewTask" class="btn btn-primary">Добавить</button></div>
		</div>';
		$htmlCode .= '</form>';
	}
	
	public function showAllTasks($tasks, &$htmlCode) 
	{
	    $htmlCode = '<table class="table table-dark">';    
	    $htmlCode .= '<thead>';    
		$htmlCode .= '<td>Номер</td>';
		$htmlCode .= '<td>Название</td>';
		$htmlCode .= '<td>Дата добавления</td>';
		$htmlCode .= '<td>Отметка об исполнении</td>';
		$htmlCode .= '<td>Действия</td>';
		$htmlCode .= '</thead>';    
		$htmlCode .= '<tbody>';    
	   foreach($tasks as $data)
	    {
	    	$htmlCode .= '<tr>';    
	    	$htmlCode .= '<td>
	    	<input type="hidden" name="managed-task">
	    	'.$data['id'].'</td>';
	    	$htmlCode .= '<td>'.$data['description'].'</td>';
	    	$htmlCode .= '<td>'.$data['date_added'].'</td>'; 
	    	/** DONE OR NOT **/
	    	$htmlCode .= '<form action="" method="get">';
	    	if($data['is_done'] == 1){
	    		$htmlCode .= '<td style="padding-left: 30px;" ><input class="form-check-input" type="radio" name="isDone" id="isDoneTrue" value="1" checked>';
				$htmlCode .= '<label style="margin-right: 30px;" class="form-check-label" for="isDone">Сделано</label><br>';
				$htmlCode .= '<input class="form-check-input" type="radio" name="isDone" id="isDoneFalse" value="0">';
				$htmlCode .= '<label style="margin-right: 30px;" class="form-check-label" for="isDoneFalse">Не сделано</label></td>';    		
	    	}else{
	    		$htmlCode .= '<td style="padding-left: 30px;" ><input class="form-check-input" type="radio" name="isDone" id="isDoneTrue" value="1">';
				$htmlCode .= '<label style="margin-right: 30px;" class="form-check-label" for="isDone">Сделано</label><br>';
				$htmlCode .= '<input class="form-check-input" type="radio" name="isDone" id="isDoneFalse" value="0" checked>';
				$htmlCode .= '<label style="margin-right: 30px;" class="form-check-label" for="isDoneFalse">Не сделано</label>
	    		</td>';
	    	}
	    	$htmlCode .= '<td>
	    	<div class="d-flex">
	    		<button style="margin-right: 30px; background:green" type="submit" name="doneNewTask" value=" '.$data['id'].' " class="btn btn-primary">OK</button>
	    		<button style="background:red" type="submit" name="deleteTask" value=" '.$data['id'].' " class="btn btn-primary">Удалить</button>
	    	</div>
	    	</td>';
	    	$htmlCode .= '</form>';
	    	$htmlCode .= '</tr>';    
	    }
	   $htmlCode .= '</tbody>';   
	   $htmlCode .= '</table>';
	}
}