<?php
namespace DBManager; 

class DBManager
{
	protected $host;
	protected $user;
	protected $pass;
	protected $db;
	protected $dbHandler; 
	
	function __construct($host, $user, $pass, $db)
	{
		$this->host = $host;
		$this->user =  $user;
		$this->pass = $pass;
		$this->db = $db;
		$this->doConnect();
	}
	
	protected function doConnect()
	{
		try
		{
			$this->dbHandler = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
		 	$this->dbHandler->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		 	$this->dbHandler->exec("set names utf8");
	 	}
	    catch (PDOException $e) 
	    {
			 print "Error!: " . $e->getMessage() . "<br/>";
			 die();
		}
	}
	
	public function getDbHandler()
	{
		$this->doConnect();
		return $this->dbHandler;
	}
	
	public function createTableTasks()
	{
		try {
			 $sql ="DROP TABLE IF EXISTS `tasks`";
		     $res = $this->dbHandler->exec($sql);
		     if($res) print("Droped `tasks` Table.\n");	
		     $sql ="CREATE TABLE `tasks` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `description` text NOT NULL,
			  `is_done` tinyint(4) NOT NULL DEFAULT '0',
			  `date_added` datetime NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8" ;
		     $res = $this->dbHandler->exec($sql);
		     if($res) print("Create `tasks` Table.\n");	
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function setNewDataToTasks($description,$is_done,$date_added) 
	{
		try {
			$stmt = $this->dbHandler->prepare("INSERT INTO tasks (description, is_done, date_added) VALUES (:description, :is_done, :date_added)");
			$stmt->bindParam(':description', $description, \PDO::PARAM_STR);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
			$stmt->bindParam(':date_added', $date_added, \PDO::PARAM_STR);
		    $res = $stmt->execute();
			var_dump($res);
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function changeTaskStatus($id,$is_done) 
	{
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET is_done = :is_done WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
		
	public function deleteTask($id) 
	{
		try {
			$stmt = $this->dbHandler->prepare("DELETE FROM tasks WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		//    			var_dump($res);
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	public function getAllTasks() 
	{
	    $sql = 'SELECT * FROM tasks';
		return $this->dbHandler->query($sql);
	}
}